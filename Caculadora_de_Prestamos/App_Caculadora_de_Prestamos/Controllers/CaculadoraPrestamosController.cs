﻿using App_Caculadora_de_Prestamos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App_Caculadora_de_Prestamos.Controllers
{
    public class CaculadoraPrestamosController : Controller
    {
        // GET: CaculadoraPrestamos
        public ActionResult Index()
        {
                return View(new CalculadoraPrestamos());
        }
       
        [HttpPost]

        public ActionResult Index(CalculadoraPrestamos calculadora)
        {
            return Redirect("~/CaculadoraPrestamos/DetallesPrestamo");
            
            
        }

        public ActionResult DetallesPrestamo()
        {
            return View(new CalculadoraPrestamos());
        }

        [HttpPost]
        public ActionResult DetallesPrestamo(CalculadoraPrestamos calculadora)
        {
            if (ModelState.IsValid)
            {
                decimal TasaM, cuota;

                //Calculo del interes mensual
                TasaM = (calculadora.Tasa / 100) / 12;

                //Calculo de la cuota mensual
                cuota = TasaM + 1;
                cuota = (decimal)Math.Pow((double)cuota, calculadora.Plazo);
                cuota = cuota - 1;
                cuota = TasaM / cuota;
                cuota = TasaM + cuota;
                cuota = calculadora.Monto * cuota;

                calculadora.cuota = Math.Round(cuota, 2);
            }
            return View(calculadora);

        }

    }
}