﻿using System.Web;
using System.Web.Mvc;

namespace App_Caculadora_de_Prestamos
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
