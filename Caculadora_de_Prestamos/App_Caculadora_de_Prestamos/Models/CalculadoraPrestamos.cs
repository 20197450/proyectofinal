﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App_Caculadora_de_Prestamos.Models
{
    public class CalculadoraPrestamos
    {
        [Required(ErrorMessage ="Se requiere llenar este campo")]
        public decimal Monto { get; set; }
        [Required(ErrorMessage = "Se requiere llenar este campo")]
        public decimal Tasa { get; set; }
        [Required(ErrorMessage = "Se requiere llenar este campo")]
        public int Plazo { get; set; }

        public decimal cuota { get; set; }

    }
}